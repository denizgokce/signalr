﻿using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SignalR.Client
{
    class Program
    {
        /// <summary>
        /// This is a basic console client application for a signalR server
        /// The parameter down here is a generic connection variable
        /// </summary>
        public static HubConnection connection;
        static void Main(string[] args)
        {
            /*
             * This is where we get the custom variables that server accepts these credentials can be arranged as needed
             */
            Console.Write("Enter Your Name : ");
            string Username = Console.ReadLine();
            Console.Write("Enter Your GroupName : ");
            string GroupName = Console.ReadLine();

            /*
             * This where we prepare the credentials to send the server
             * The Server accepts these credentials as querystring and thats how queryString built dynamicly in .net client
             */
            var querystringData = new Dictionary<string, string>();
            querystringData.Add("Username", Username);
            querystringData.Add("GroupName", GroupName);

            /*
             * This is url of a signalR server
             */
            string url = "http://localhost:5000/";

            connection = new HubConnection(url, querystringData);
            IHubProxy _hub = connection.CreateHubProxy("ChatHub");
            connection.Start().ContinueWith(task =>
            {

                if (task.IsCompleted)
                {

                    Console.Clear();
                    Console.WriteLine("Username : " + Username);
                    Console.WriteLine("Group Name : " + GroupName + "\n");
                    Console.WriteLine("You Have Connected To The Server.. \nNote: To leave just type \"exit\" \n");

                }
                else if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error opening the connection:{0}", task.Exception.GetBaseException());
                }

            }).Wait();


            _hub.On<string>("ReceiveMessage", (x) =>
            {
                Console.WriteLine(x);
            });

            string line = null;
            while ((line = System.Console.ReadLine()) != null)
            {
                if (line != "exit")
                {
                    _hub.Invoke("BroadCastMessage", line, GroupName).Wait();
                    ClearCurrentConsoleLine();
                }
                else
                {
                    connection.Stop();
                    Environment.Exit(0);
                }
            }
            Console.Read();
        }

        public static void ClearCurrentConsoleLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
}

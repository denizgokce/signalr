﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SignalR.Server
{
    /// <summary>
    /// 
    /// Author : Deniz Gökçe
    /// Date : 18.12.2015
    /// 
    ///     This is the Hub Class, Hub is one of the main class in SignalR this class manages every request that comes from the
    /// client you can configure your Hub any way you want, this Hub is spesific for chatserver.
    ///     Hub class has Three main abstract method that should be overide, Those are;
    /// 1) OnConnected ~ this method is fired when client first connect attempt Hub recieves some client Information here
    /// 2) On Reconnected ~ this method is fired when the connected client tries the connect again
    /// 3) On Disconnected ~ this method is fired when connected client disconnects from the server this could be planned or 
    /// unplanned the API has hooks fpr unplanned disconnect state like for a web client refreshes the client page.
    /// 
    /// </summary>
    public class ChatHub : Hub
    {
        /// <summary>
        /// You need to manage the clients in your server so you can redirect the requests
        /// o	You must define a unique property to the clients such as UserID so you can keep the online clients
        /// o	This list is the online user list for this chatserver
        /// </summary>

        #region Defn
        public static List<Model.OnlineUser> OnlineUsers = new List<Model.OnlineUser>();
        #endregion

        /// <summary>
        /// This the method that fires when client connection start
        /// Important thing is you need to call a client side method because the client needs at least one custom method 
        /// to recieve connection state ~ started or failed
        /// Context is Hub's own instance that is created when hub is initialized it assigns a unique connection id to every
        /// client when you called Context.ConnectionId it means the client that is calling the hub
        /// </summary>
        /// <returns></returns>
        public override Task OnConnected()
        {
            /*
             * This the how you handle the connection values as query string to send a inital value to the server the client
             * add query string values to the connection inctance at the client side
             */
            var Username = Context.QueryString["Username"];
            var GroupName = Context.QueryString["GroupName"];


            /*
             *
             */
            var existRecord = OnlineUsers.Where(x => x.Username == Username).FirstOrDefault();
            if (existRecord == null)
            {
                Model.OnlineUser newUser = new Model.OnlineUser()
                {
                    Username = Username,
                    ConnectionID = Context.ConnectionId,
                    groupName = GroupName
                };
                OnlineUsers.Add(newUser);

                Groups.Add(newUser.ConnectionID, GroupName);

                Console.WriteLine(Username + " has connected to the server.");

                Clients.Group(GroupName).ReceiveMessage(newUser.Username + " has joined this room..");
                return base.OnConnected();
            }
            else
            {
                existRecord.ConnectionID = Context.ConnectionId;
                existRecord.groupName = GroupName;

                Console.WriteLine(Username + " has connected to the server.");

                Clients.Group(GroupName).ReceiveMessage(existRecord.Username + " has joined this room..");
                return base.OnConnected();
            }
        }
        /// <summary>
        /// This Method is a custom method, it broadcasts the message that is sent from a client in this method message can be
        /// handled easily, it can be saved to database or it can trigger a push notification etc.
        /// </summary>
        /// <param name="message">This parameter contains the message itself</param>
        /// <param name="groupName">This parameter contains the groupName which defines message target altough it can be
        /// stored in onlineUser list but it is up to you..</param>
        public void BroadCastMessage(string message, string groupName)
        {
            var usr = OnlineUsers.Where(x => x.ConnectionID == Context.ConnectionId).FirstOrDefault();
            Console.WriteLine(usr.Username + " write in the room \"" + usr.groupName + "\" message \"" + message + "\"");
            Clients.Group(groupName).ReceiveMessage(usr.Username + " : " + message);
        }
        /// <summary>
        /// This method is one of abstract method of this framework it automaticly fires when a client disconnects from server
        /// planned or unplanned what needs to ben done here is remove the user from onlineUser List
        /// </summary>
        /// <param name="stopCalled">This parameter sends automaticly from client if a client wants to disconnect from server 
        /// this parameter is always true</param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            if (stopCalled)
            {
                // We know that Stop() was called on the client,
                // and the connection shut down gracefully.
                var usr = OnlineUsers.Where(x => x.ConnectionID == Context.ConnectionId).FirstOrDefault();
                if (usr != null)
                {
                    Console.WriteLine(usr.Username + " has Disconnected from the server");
                    Clients.Group(usr.groupName).ReceiveMessage(usr.Username + " has left your room..");
                    OnlineUsers.Remove(usr);
                }
            }
            else
            {
                // This server hasn't heard from the client in the last ~35 seconds.
                // If SignalR is behind a load balancer with scaleout configured, 
                // the client may still be connected to another SignalR server.
            }

            return base.OnDisconnected(stopCalled);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
    }
}

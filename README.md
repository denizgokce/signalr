# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is Simple SignalR Chat Project for Demonstration of SignalR Remote Procedure Call Framework
* Solution Contains WebServer, WebClient, ConsoleServer and ConsoleClient
* 1.0
* [Learn Markdown](https://bitbucket.org/DenizGokce/windowshopper)
* SignalR Web Chat Server URL [SignalR Chat Server](http://signalr-chat-server.azurewebsites.net/)
* SignalR Web Chat Client URL [SignalR Chat Client](http://signalr-chat-client.azurewebsites.net/)

### Architecture and Frameworks That Project Includes ###

* ASP.NET SignalR RPC Framework [SignalR Official Page](https://www.asp.net/signalr/)

### How to Use? ###

* [SignalR Chat Client](http://signalr-chat-client.azurewebsites.net/) Open up a client
* Choose an alias an create a chat room
* Ask anyone else to connect your with their own alias
* Then start chattig!


### Who do I talk to? ###

* Deniz Gokce
* denizgokce93@gmail.com

## There is no Database Implementation behind your so the chat will be gone after you close the tab! ##
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalR.WebServer.Model
{
    public partial class OnlineUser
    {

        public OnlineUser()
        {

        }
        public string ConnectionID { get; set; }
        public string Username { get; set; }
        public string groupName { get; set; }
    }
}
